package main

import (
	"fmt"
	"net/http"
)

func main() {
	fmt.Printf("hello, world\n")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "hey, you've requested: %s\n", r.URL.Path)
	})
	http.ListenAndServe(":8000", nil)
}
