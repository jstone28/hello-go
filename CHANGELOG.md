## 0.0.6 [06-24-2019]
* Bug fixes and performance improvements

See commit e607e39

---

## 0.0.5 [06-24-2019]
* Bug fixes and performance improvements

See commit 17ebae1

---

## v0.0.4 [06-24-2019]
* Bug fixes and performance improvements

See commit 9375e59

---

## v0.0.3 [06-24-2019]
* Bug fixes and performance improvements

See commit e9db665

---

## v0.0.2 [06-24-2019]
* Bug fixes and performance improvements

See commit 0fb0e95

---
