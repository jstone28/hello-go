FROM golang:alpine AS build-stage
COPY . /go/src/hello-go
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o hello-go /go/src/hello-go

FROM scratch
COPY --from=build-stage /go/hello-go .
EXPOSE 8000
CMD ["./hello-go"]
